const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const path = require('path');
const mqtt = require('mqtt');
const PORT = process.env.PORT || 4001;
const CONNECTION_MESSAGE = "A client connected";
const MQTT_ADDRESS = 'mqtt://localhost:1883';
const CHART_DATA = 'chartData';
const CHART_ENABLED = 'chartEnabled';
const CHART_DISABLED = 'chartDisabled';
const CONFIG_DATA = 'configData';

const app = express();
app.use(express.static(path.join(__dirname, 'public')));
app.use(function (req, res, next) {
    res.header("Content-Security-Policy", "script-src 'self' http://localhost:4001");
    return next();
});

const mqttClient = mqtt.connect(MQTT_ADDRESS);
let enabledCharts = {};
let topicToCharts = {};

mqttClient.on('message', handleMessage);

function handleMessage(topic, message) {
    const data = JSON.parse(message);
    const parsed_data = data.data;
    if (topic in topicToCharts) {
        topicToCharts[topic].forEach(chartId => {
            io.sockets.emit(CHART_DATA, {
                "chartName": chartId,
                "data": parsed_data
            });
        });
    }
}

const server = http.createServer(app);
const io = socketIo(server);
io.on('connection', (client) => {
    console.log(CONNECTION_MESSAGE);
    client.on(CHART_ENABLED, handleChartEnable);
    client.on(CHART_DISABLED, handleChartDisable);
    client.on(CONFIG_DATA, handleConfigData);
});

function handleChartEnable(chartId) {
    console.log(`Chart ${chartId} has been enabled.`);
    enabledCharts[chartId] = true;
}

function handleChartDisable(chartId) {
    console.log(`Chart ${chartId} has been disabled.`);
    enabledCharts[chartId] = false;
}

function handleConfigData(dataString) {
    const data = JSON.parse(dataString);
    if (!(data.value in topicToCharts)) {
        topicToCharts[data.value] = [];
    }
    topicToCharts[data.value].push(data.chartId);

    if (data.origin === 'mqtt-button') {
        subscribeToMqttTopic(data.value);
    }
}

function subscribeToMqttTopic(topic) {
    mqttClient.subscribe(topic, function (err) {
        if (!err) {
            console.log('Successfully subscribed to the topic');
        }
    });
}

server.listen(PORT, () => console.log(`Listening on port ${PORT}`));