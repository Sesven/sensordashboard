const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const SerialPort = require('serialport');
const Readline = require('@serialport/parser-readline');

// Setup express
const port = process.env.PORT || 4001;
const app = express();
const server = http.createServer(app);

// Setup socket.io
const io = socketIo(server);

let interval;

io.on('connection', (socket) => {
  console.log('New client connected');
  if (interval) {
    clearInterval(interval);
  }
  interval = setInterval(() => getApiAndEmit(socket), 1000); // 1000ms between each new data point
  socket.on('disconnect', () => {
    console.log('Client disconnected');
    clearInterval(interval);
  });
});

const getApiAndEmit = (socket) => {
  const port = new SerialPort('/dev/tty.usbmodem14101', {
    baudRate: 57600,
  });

  const parser = port.pipe(new Readline({ delimiter: '\r\n' }));

  parser.on('data', (data) => {
    // Emit the data from the serial port over the socket
    socket.emit('FromAPI', data);
  });
};

server.listen(port, () => console.log(`Listening on port ${port}`));