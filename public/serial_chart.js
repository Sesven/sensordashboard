// Establish a Socket.io connection
const socket = io('http://localhost:4001');

// Initialize a new Chart
const ctx = document.getElementById('myChart').getContext('2d');
const chart = new Chart(ctx, {
  type: 'line',
  data: {
    labels: [],
    datasets: [{
      data: [],
      label: 'Real-Time Data',
      borderColor: '#3e95cd',
      fill: false
    }]
  },
});

function toggleOverlay(element) {
    element.style.display = 'none';
}

const MAX_DATA_POINTS = 2;

function makeSine(frequency, samplesPerSecond, duration) {
  const data = [];
  const sampleIncrement = 1/samplesPerSecond; // time for one sample

  for (let t = 0; t < duration; t += sampleIncrement) {
    const value = Math.sin(2 * Math.PI * frequency * t);
    data.push({ t, value });
  }

  return data;
}
const sineWave = makeSine(50, 120, 1);

// Update the Chart data with the data received from the server
socket.on('FromAPI', data => {

  sineWave.forEach(dataPoint => {
    chart.data.labels.push(dataPoint.t.toFixed(2));  // Use time as the label, to 2 decimal places
    chart.data.datasets[0].data.push(dataPoint.value);
  });

    if (chart.data.labels.length > MAX_DATA_POINTS) {
    chart.data.labels.shift();  // Removes the first item
    chart.data.datasets[0].data.shift();  // Removes the first item
  }
  chart.update();
});

