const chartStates = {
    'chart1': false,
    'chart2': false,
    'chart3': false,
    'chart4': false
};

let socket;



function createButtons(parentElement, chartId) {
    // Create the 'MQTT' button
    const mqttButton = document.createElement('button');
    mqttButton.className = 'mqtt-button'
    mqttButton.innerText = 'MQTT';
    mqttButton.connection_context = "Subscription topic";
    mqttButton.onclick = function () {
        createTextBox(parentElement, mqttButton.connection_context, mqttButton.className, chartId);
    };
    mqttButton.replaceWith(mqttButton.cloneNode(true));

    // Create the 'Serial' button
    const serialButton = document.createElement('button');
    serialButton.className = 'serial-button'
    serialButton.innerText = 'Serial';
    serialButton.connection_context = "Serial path"
    serialButton.onclick = function () {
        createTextBox(parentElement, serialButton.connection_context, serialButton.className, chartId);
    };

    const disconnectButton = document.createElement('button');
    disconnectButton.className = "disconnect-button";
    disconnectButton.innerText = 'Disconnect';
    disconnectButton.style.marginRight = '63px';
    disconnectButton.onclick = function () {
        socket.emit('chartDisabled', chartId);
        chartStates[chartId] = false;
    };
    disconnectButton.replaceWith(serialButton.cloneNode(true));

    const connectButton = document.createElement('button');
    connectButton.className = "connect-button";
    connectButton.innerText = 'Connect';
    connectButton.onclick = function () {
        socket.emit('chartEnabled', chartId);
        chartStates[chartId] = true;
    };
    connectButton.replaceWith(serialButton.cloneNode(true));

    // Append the buttons to the parent element
    parentElement.appendChild(mqttButton);
    parentElement.appendChild(serialButton);
    parentElement.appendChild(disconnectButton);
    parentElement.appendChild(connectButton);
}

function createTextBox(parentElement, labelText, buttonClass, chartId) {

    let test;
    for (let i = 0; i < parentElement.children.length; i++) {
        test = parentElement.children[i]
        console.log(parentElement.children);
    }
    // Remove any existing wrapper from the parent element
    const existingTextBox = parentElement.querySelector('connection-config-box');
    if (existingTextBox) {
        // parentElement.removeChild(existingTextBox.previousElementSibling); // Remove the corresponding label
        parentElement.removeChild(existingTextBox); // Remove the text box
    }
    // Create wrapper for the label and the text box
    const wrapper = document.createElement('connection-config-box');
    wrapper.style.position = 'absolute';
    wrapper.style.left = '50%';
    wrapper.style.top = '50%';
    wrapper.style.transform = 'translate(-50%, -50%)';
    wrapper.style.display = 'flex';
    wrapper.style.flexDirection = 'column';
    wrapper.style.alignItems = 'center';
    wrapper.style.justifyContent = 'center';
    wrapper.style.backgroundColor = 'rgba(184,184,184,0.7)';
    wrapper.style.padding = '10px';
    wrapper.style.borderRadius = '5px';

    // Create the label
    const label = document.createElement('label');
    label.innerText = labelText;

    // Create the text box
    const textBox = document.createElement('input');
    textBox.type = 'text';

    const submitButton = document.createElement('button');
    submitButton.innerText = 'Submit';
    submitButton.onclick = function () {
        const textBoxValue = textBox.value;
        if (textBoxValue.length === 0) {
            wrapper.remove();
            return;
        }
        socket.emit('configData', JSON.stringify({value: textBoxValue, origin: buttonClass, chartId: chartId}));
        wrapper.remove()
    };

    textBox.addEventListener('keydown', function (event) {
        if (event.key === 'Enter') {
            event.preventDefault(); //this is to prevent form submission in case your buttons and input text are in form element

            // Perform click on the submit button
            submitButton.click();
        }
    });

    // Append the label and text box to the wrapper
    wrapper.appendChild(label);
    wrapper.appendChild(textBox);
    wrapper.appendChild(submitButton)

    // Append the wrapper to the parent element
    parentElement.appendChild(wrapper);
}


document.addEventListener('DOMContentLoaded', (event) => {
    socket = io.connect('http://localhost:4001');

    const chartNames = ['chart1', 'chart2', 'chart3', 'chart4'];
    let charts = {};

    for (const chartName of chartNames) {
        const ctx = document.getElementById(chartName).getContext('2d');
        charts[chartName] = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [],
                datasets: [{
                    data: [],
                    label: 'Value',
                    borderColor: '#3e95cd',
                    fill: false
                }
                ]
            },
            options: {
                animation: {
                    duration: 0 // This will turn off the animation
                },
                // scales: {
                //     y: {
                //         suggestedMin: -2,  // The Y axis will start from -1
                //         suggestedMax: 2  // The Y axis will end at 1
                //     },
                // },
                responsive: true,  // This will make sure that your chart responsively resizes
            }
        });
    }

    const MAX_DATA_POINTS = 256;

    socket.on('chartData', handleData);


    function handleData(packet) {
        if (!chartStates[packet.chartName]) {
            return;
        }
        const chart = charts[packet.chartName];

        if (Array.isArray(packet.data) && packet.data.length > 1) {
            // handle array data
            chart.data.labels = Array.from({length: packet.data.length}, (_, i) => i);
            chart.data.datasets[0].data = packet.data;
        } else {
            // handle single-value updates
            const dataPoint = Array.isArray(packet.data) ? packet.data[0] : packet.data;
            chart.data.labels.push(new Date().toLocaleTimeString());
            chart.data.datasets[0].data.push(dataPoint);

            // maintain array length to MAX_DATA_POINTS
            if (chart.data.labels.length > MAX_DATA_POINTS) {
                chart.data.labels.shift();
                chart.data.datasets[0].data.shift();
            }
        }

        chart.update();
    }
});
