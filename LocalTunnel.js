var localtunnel = require('localtunnel');
const port = 4001;

(async () => {

  const tunnel = await localtunnel({ port: port });

  // the assigned public url for your tunnel
  // i.e. https://abcdefgjhij.localtunnel.me
  console.log(tunnel.url);

  tunnel.on('close', () => {
    // When the tunnel is closed
    console.log("LocalTunnel is closed");
  });
})();